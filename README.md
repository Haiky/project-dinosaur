# project-dinosaur

Bienvenue sur un projet de test du Framework VueJs.

### Objectifs :

 * Comprendre les interactions entre les différents components.
 * Obtenir un site visuellement agréable.
 * Données statiques.
 * Mise en place de Tests de Non Regression avec Cypress.